# SearchPosts

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 13.2.0.

## install  dependencies

Run `npm i`

## Development server

Run `npm run start` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.


## Build

Run `npm run build` to build the project. The build artifacts will be stored in the `dist/search-posts` 

Run `npm run build:components` to build web components. The build js file will be stored in the root of the app beside src folder `post-components/post-component.js`,
and can be extracted and put on another angular application into assets folder `assets/post-components.js`, also script is required in `index.html` file - ` <script type="text/javascript" src="./assets/post-component.js"></script>`,and custom element schema `schemas: [CUSTOM_ELEMENTS_SCHEMA]` in app module,
the components can be used under tags `<post-component>` `<search-component>`
