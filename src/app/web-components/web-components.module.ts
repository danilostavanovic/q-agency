import { BrowserModule } from '@angular/platform-browser';
import { CommonModule } from '@angular/common';
import { DoBootstrap, Injector, NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { createCustomElement } from '@angular/elements';
import { PostComponent } from '../posts/components/post/post.component';
import { SearchFieldComponent } from '../posts/components/search-field/search-field.component';

@NgModule({
  imports: [CommonModule, BrowserModule, ReactiveFormsModule],
  exports: [PostComponent, SearchFieldComponent],
  declarations: [PostComponent, SearchFieldComponent],
  providers: [],
})
export class WebComponentsModule implements DoBootstrap {
  constructor(private injector: Injector) {}

  ngDoBootstrap(): void {
    const post = createCustomElement(PostComponent, {
      injector: this.injector,
    });
    const searchField = createCustomElement(SearchFieldComponent, {
      injector: this.injector,
    });
    customElements.define('post-component', post);
    customElements.define('search-component', searchField);
  }
}
