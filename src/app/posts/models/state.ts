import { IUser } from './user';
import { IPost } from './post';
import { IDictionary } from './dictionary';
import { IComment } from './comment';

export interface IState {
  users: IDictionary<IUser> | null;
  posts: IPost[];
  filteredPosts: IPost[];
  comments: IDictionary<IComment[]> | null;
  isLoading: boolean;
  isFilterActive: boolean;
  error: string;
}
