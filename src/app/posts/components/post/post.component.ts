import {
  ChangeDetectionStrategy,
  Component,
  Input,
  OnInit,
} from '@angular/core';
import { IPost } from '../../models/post';
import { IUser } from '../../models/user';
import { IComment } from '../../models/comment';

@Component({
  selector: 'app-post',
  templateUrl: './post.component.html',
  styleUrls: ['./post.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class PostComponent implements OnInit {
  @Input()
  public post: IPost;

  @Input()
  public user: IUser;

  @Input()
  public comments: IComment[];

  @Input()
  public showCommentDetails: boolean;

  public initials: string;

  ngOnInit(): void {
    if (this.user) {
      this.initials = this.getInitialsForAvatar(this.user.name);
    }
  }

  public getInitialsForAvatar(name: string): string {
    const fullName = name.split(' ');
    const initials = `${fullName.shift().charAt(0)}${fullName.pop().charAt(0)}`;
    return initials.toUpperCase();
  }

  public trackByFn(index, post: IComment): number | string {
    return post.id;
  }
}
