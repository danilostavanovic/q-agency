import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  Input,
  OnDestroy,
  OnInit,
  Output,
} from '@angular/core';
import { FormControl } from '@angular/forms';
import { Subject } from 'rxjs';
import { debounceTime, distinctUntilChanged, takeUntil } from 'rxjs/operators';

@Component({
  selector: 'app-search-field',
  templateUrl: './search-field.component.html',
  styleUrls: ['./search-field.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SearchFieldComponent implements OnInit, OnDestroy {
  @Input()
  public placeholder: string;

  @Output()
  public filterChange: EventEmitter<string> = new EventEmitter<string>();

  public searchControl: FormControl = new FormControl('');

  private destroySub$: Subject<any> = new Subject();

  ngOnInit(): void {
    this.searchControl.valueChanges
      .pipe(
        takeUntil(this.destroySub$),
        distinctUntilChanged(),
        debounceTime(500)
      )
      .subscribe({
        next: (searchTerm) => this.filterChange.emit(searchTerm),
      });
  }

  ngOnDestroy(): void {
    this.destroySub$.next();
    this.destroySub$.complete();
  }
}
