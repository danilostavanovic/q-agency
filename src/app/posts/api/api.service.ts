import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { IUser } from '../models/user';
import { IPost } from '../models/post';
import { IComment } from '../models/comment';

@Injectable()
export class ApiService {
  private readonly BASE_URL = 'https://jsonplaceholder.typicode.com';

  constructor(private http: HttpClient) {}

  public getFakeUsers(): Observable<IUser[]> {
    return this.http.get<IUser[]>(`${this.BASE_URL}/users`);
  }

  public getFakePosts(): Observable<IPost[]> {
    return this.http.get<IPost[]>(`${this.BASE_URL}/posts`);
  }

  public getFakeComments(): Observable<IComment[]> {
    return this.http.get<IComment[]>(`${this.BASE_URL}/comments`);
  }
}
