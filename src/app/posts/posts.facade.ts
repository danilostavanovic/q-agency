import { Injectable } from '@angular/core';
import { ApiService } from './api/api.service';
import { combineLatest, forkJoin, Observable } from 'rxjs';
import { Store } from './state/store';
import { IState } from './models/state';
import { map } from 'rxjs/operators';
import { IPost } from './models/post';
import { IUser } from './models/user';
import { IComment } from './models/comment';
import { IDictionary } from './models/dictionary';

@Injectable()
export class PostsFacade {
  constructor(private apiService: ApiService, private store: Store) {}

  private observableSource$ = combineLatest([
    this.store.select('isLoading'),
    this.store.select('error'),
    this.store.select('posts'),
    this.store.select('filteredPosts'),
    this.store.select('users'),
    this.store.select('isFilterActive'),
    this.store.select('comments'),
  ]);

  public state$: Observable<IState> = this.observableSource$.pipe(
    map(
      ([
        isLoading,
        error,
        posts,
        filteredPosts,
        users,
        isFilterActive,
        comments,
      ]) => {
        return {
          isLoading,
          error,
          posts,
          filteredPosts,
          users,
          isFilterActive,
          comments,
        };
      }
    )
  );

  public fetchDataAndPutIntoStore(): void {
    this.setState({ isLoading: true });
    forkJoin([
      this.apiService.getFakePosts(),
      this.apiService.getFakeUsers(),
      this.apiService.getFakeComments(),
    ])
      .pipe(
        map(([posts, users, comments]) => {
          return {
            posts,
            users: this.transformUserDto(users),
            comments: this.transformCommentsDto(comments),
          };
        })
      )
      .subscribe({
        next: ({ posts, users, comments }) => {
          const filteredPosts: IPost[] = [...posts];
          this.setState({
            posts,
            users,
            comments,
            filteredPosts,
            isLoading: false,
          });
        },
        error: () => {
          this.setState({
            error: 'Ups something went wrong',
            isLoading: false,
          });
        },
      });
  }

  public onFilterPost(searchTerm: string, state: IState): void {
    this.setState({ isFilterActive: true });
    const searchCriteria = searchTerm.toLowerCase().split(' ').filter(Boolean);
    const filteredPosts = state.posts.filter((post) => {
      const user = state.users[post.userId];
      const userFilter = this.getUserFilter(user);
      return searchCriteria.some((criteria) => userFilter.includes(criteria));
    });
    this.setState({
      filteredPosts: searchTerm !== '' ? filteredPosts : state.posts,
      isFilterActive: false,
    });
  }

  public setState(dataForState: Partial<IState>): void {
    Object.entries(dataForState).forEach(([key, value]) => {
      this.store.set(key, value);
    });
  }

  private getUserFilter(user: IUser): string {
    return [
      user.name,
      user.email,
      user.username,
      user.phone,
      user.website,
      ...Object.values(user.address),
      ...Object.values(user.company),
    ]
      .join(' ')
      .toLowerCase();
  }

  private transformUserDto(users: IUser[]): IDictionary<IUser> {
    return users.reduce((obj, user) => {
      const { id, ...dataFromUser } = user;
      obj[id] = dataFromUser;
      return obj;
    }, {});
  }

  private transformCommentsDto(comments: IComment[]): IDictionary<IComment[]> {
    return comments.reduce((obj, comment) => {
      const key = comment.postId;
      if (!obj[key]) {
        obj[key] = [];
      }
      obj[key].push(comment);
      return obj;
    }, {});
  }
}
