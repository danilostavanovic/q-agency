import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';

import { distinctUntilChanged, pluck } from 'rxjs/operators';
import { IState } from '../models/state';

const state: IState = {
  isLoading: false,
  isFilterActive: false,
  error: '',
  posts: [],
  filteredPosts: [],
  users: null,
  comments: null,
};

@Injectable()
export class Store {
  private subject = new BehaviorSubject<IState>(state);
  private store = this.subject.asObservable().pipe(distinctUntilChanged());

  public select(name: string): Observable<any> {
    return this.store.pipe(pluck(name));
  }

  public set(name: string, changeStateValue: any): void {
    this.subject.next({
      ...this.value,
      [name]: changeStateValue,
    });
  }
  private get value(): IState {
    return this.subject.value;
  }
}
