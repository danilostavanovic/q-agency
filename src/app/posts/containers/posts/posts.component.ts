import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { PostsFacade } from '../../posts.facade';
import { IState } from '../../models/state';
import { IPost } from '../../models/post';

@Component({
  selector: 'app-posts',
  templateUrl: './posts.component.html',
  styleUrls: ['./posts.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class PostsComponent implements OnInit {
  constructor(public facade: PostsFacade, private router: Router) {}

  ngOnInit(): void {
    this.facade.fetchDataAndPutIntoStore();
  }

  public goToPostReview(id: number): void {
    this.router.navigate(['post', id]).then();
  }

  public onFilterChange(term: string, state: IState): void {
    this.facade.onFilterPost(term, state);
  }

  public trackByFn(index, post: IPost): number | string {
    return post.id;
  }
}
