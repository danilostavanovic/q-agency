import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { IPost } from '../../models/post';
import { combineLatest, Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { PostsFacade } from '../../posts.facade';

@Component({
  selector: 'app-post-review',
  templateUrl: './post-review.component.html',
  styleUrls: ['./post-review.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class PostReviewComponent implements OnInit {
  constructor(private router: ActivatedRoute, public facade: PostsFacade) {}

  ngOnInit(): void {
    this.facade.fetchDataAndPutIntoStore();
  }

  public get getPost$(): Observable<IPost> {
    return combineLatest([this.router.params, this.facade.state$]).pipe(
      map(([param, state]) => {
        return state.posts.find((post) => post.id === Number(param.id));
      })
    );
  }
}
