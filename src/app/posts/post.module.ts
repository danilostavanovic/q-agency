import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { PostsComponent } from './containers/posts/posts.component';
import { PostComponent } from './components/post/post.component';
import { PostRoutingModule } from './post-routing.module';
import { ApiService } from './api/api.service';
import { PostsFacade } from './posts.facade';
import { SearchFieldComponent } from './components/search-field/search-field.component';
import { ReactiveFormsModule } from '@angular/forms';
import { Store } from './state/store';
import { PostReviewComponent } from './containers/post-review/post-review.component';

const COMPONENTS = [
  PostsComponent,
  PostComponent,
  SearchFieldComponent,
  PostReviewComponent,
];

const MODULES = [
  PostRoutingModule,
  HttpClientModule,
  CommonModule,
  ReactiveFormsModule,
];

const PROVIDERS = [ApiService, PostsFacade, Store];

@NgModule({
  declarations: [...COMPONENTS],
  imports: [...MODULES],
  exports: [...COMPONENTS],
  providers: [...PROVIDERS],
})
export class PostModule {}
