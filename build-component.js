const fs = require('fs-extra');
const concat = require('concat');

const build = async () => {
  const files = [
    './dist/search-posts/runtime.js',
    './dist/search-posts/polyfills.js',
    './dist/search-posts/main.js',
  ];

  await fs.ensureDir('post-components');
  await concat(files, 'post-components/post-component.js');
};
build();
